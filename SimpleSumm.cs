﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW9_Multithread
{
    class SimpleSumm : IArrayCalc
    {
        public int GetSum(int[] array)
        {
            var sum = 0;
            for (var i = 0; i < array.Length; i++)
                sum += array[i];
            return sum;

        }
    }
}
