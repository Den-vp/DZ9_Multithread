﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
namespace HW9_Multithread
{
    class Program
    {
        static void Main(string[] args)
        {
            var testList = new List<int>() { 100_000, 1_000_000, 10_000_000 };
            var simpleSumm = new SimpleSumm();
            var linqSumm = new LinqSumm();
            var multiThreadSumm = new MultiThreadSumm(Environment.ProcessorCount);
            int sum = 0;
            foreach (int test in testList)
            {
                var arr = new int[test];
                for (int i = 0; i < test; i++)
                    arr[i] = 1;
                                
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                sum = simpleSumm.GetSum(arr);
                stopwatch.Stop();
                Console.WriteLine($"Послед. Sum = {sum} длительность исполнения = {stopwatch.ElapsedMilliseconds}");
                stopwatch.Reset();
                stopwatch.Start();
                sum = multiThreadSumm.GetSum(arr);
                stopwatch.Stop();
                Console.WriteLine($"Паралл. Sum = {sum} длительность исполнения = {stopwatch.ElapsedMilliseconds}");
                stopwatch.Reset();
                stopwatch.Start();
                sum = linqSumm.GetSum(arr);
                stopwatch.Stop();
                Console.WriteLine($"PLINQ. Sum = {sum} длительность исполнения = {stopwatch.ElapsedMilliseconds}");
            }
            
        }
    }
}
